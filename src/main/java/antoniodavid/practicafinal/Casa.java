package antoniodavid.practicafinal;

/**
 *
 * @author Antonio David
 * @version 1.0
 */
public class Casa {
    private int id;
    private boolean tiene_jardin;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the tiene_jardin
     */
    public boolean getTiene_jardin() {
        return tiene_jardin;
    }

    /**
     * @param tiene_jardin the tiene_jardin to set
     */
    public void setTiene_jardin(boolean tiene_jardin) {
        this.tiene_jardin = tiene_jardin;
    }
    
    
}

